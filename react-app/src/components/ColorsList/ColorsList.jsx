import React, { useState } from "react";
import style from "./ColorList.module.css";

import {
  colorIphone1,
  colorIphone2,
  colorIphone3,
  colorIphone4,
  colorIphone5,
  colorIphone6,
} from "./../../images";

const colors = [
  {
    id: "iphone-red",
    image: colorIphone1,
    model: "Apple iPhone 13",
    color: "красный",
  },
  {
    id: "iphone-green",
    image: colorIphone2,
    title: "Apple iPhone 13",
    color: "зелёный",
  },
  {
    id: "iphone-pink",
    image: colorIphone3,
    title: "Apple iPhone 13",
    color: "розовый",
  },
  {
    id: "iphone-blue",
    image: colorIphone4,
    title: "Apple iPhone 13",
    color: "синий",
  },
  {
    id: "iphone-white",
    image: colorIphone5,
    title: "Apple iPhone 13",
    color: "белый",
  },
  {
    id: "iphone-black",
    image: colorIphone6,
    title: "Apple iPhone 13",
    color: "чёрный",
  },
];

const ColorItem = ({ id, image, title, color, onClick }) => {
  return (
    <>
      <input
        className={style.radioIphone}
        type="radio"
        name="iphone-radio"
        id={id}
      />
      <label
        className={style.radioIphone__label}
        htmlFor={id}
        onClick={onClick}
      >
        <img
          className={style.radioIphone__img}
          src={image}
          alt={title + " " + color}
        />
      </label>
    </>
  );
};

export const ColorsList = () => {
  const [activeColor, setActiveColor] = useState("синий");

  const handleClick = (color) => {
    setActiveColor(color);
  };

  return (
    <div className={style.collorItem}>
      <h3 className={style.collorItem__title}>Цвет товара: {activeColor}</h3>
      <div className={style.collorList}>
        {colors.map((item) => (
          <ColorItem
            id={item.id}
            image={item.image}
            title={item.title}
            color={item.color}
            key={item.id}
            onClick={() => handleClick(item.color)}
          />
        ))}
      </div>
    </div>
  );
};
