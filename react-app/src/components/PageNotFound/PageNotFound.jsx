import { Link } from "react-router-dom";

import { Header } from "../Header/Header";
import { Footer } from "../Footer/Footer";

import styles from "./PageNotFound.module.css";
import linkStyles from "../Link/Link.module.css";

export const PageNotFound = () => {
  return (
    <div className={styles.wrapper}>
      <Header />
      <main className={styles.content}>
        <h1>404</h1>
        <Link className={linkStyles.link} to="/product">
          Перейти на страницу товара
        </Link>
      </main>
      <Footer />
    </div>
  );
};
