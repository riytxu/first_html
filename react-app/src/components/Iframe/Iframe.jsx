export const Iframe = ({ className, src, height, title }) => {
  return (
    <iframe
      className={className}
      src={src}
      frameBorder="1"
      height={height}
      title={title}
    ></iframe>
  );
};
