import styles from "./ProductPhoto.module.css";

import {
  ProductPhoto1,
  ProductPhoto2,
  ProductPhoto3,
  ProductPhoto4,
  ProductPhoto5,
} from "../../images";

export const ProductPhoto = () => {
  return (
    <section className={styles.productPhoto}>
      <img
        className={styles.productPhoto__item}
        src={ProductPhoto1}
        alt="Apple iPhone 13 'голубой'"
      />
      <img
        className={styles.productPhoto__item}
        src={ProductPhoto2}
        alt="Apple iPhone 13 'голубой'"
      />
      <img
        className={styles.productPhoto__item}
        src={ProductPhoto3}
        alt="Apple iPhone 13 'голубой'"
      />
      <img
        className={styles.productPhoto__item}
        src={ProductPhoto4}
        alt="Apple iPhone 13 'голубой'"
      />
      <img
        className={styles.productPhoto__item}
        src={ProductPhoto5}
        alt="Apple iPhone 13 'голубой'"
      />
    </section>
  );
};
