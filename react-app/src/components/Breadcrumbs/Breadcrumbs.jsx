import React from "react";

import styled from "styled-components";

import { Link } from "../Link/Link";

const Nav = styled.nav({
  display: "flex",
  "flex-wrap": "wrap",
  padding: "8px 0px",
  gap: "4px",
  "font-size": "16px",
});

const BreadcrumbsArr = [
  ["electronics", "Электроника"],
  ["smartphones-and-gadgets", "Смартфоны и гаджеты"],
  ["mobile-phones", "Мобильные телефоны"],
  ["apple", "Apple"],
];

export const Breadcrumbs = () => {
  return (
    <Nav>
      {BreadcrumbsArr.map((item, index) => (
        <Link href={`#${item[0]}`} key={index}>
          {index <= item.length ? `${item[1]} >` : `${item[1]}`}
        </Link>
      ))}
    </Nav>
  );
};
