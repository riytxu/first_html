import cn from "classnames";

import { Header } from "../Header/Header";
import { Footer } from "../Footer/Footer";
import { ColorsList } from "../ColorsList/ColorsList";
import { MemoryConfList } from "../MemoryConfList/MemoryConfList";
import { ReviewList } from "../Review/Review";
import { Breadcrumbs } from "../Breadcrumbs/Breadcrumbs";
import { Form } from "../Form/Form";
import { Sidebar } from "../Sidebar/Sidebar";
import { ProductName } from "../ProductName/ProductName";
import { ProductPhoto } from "../ProductPhoto/ProductPhoto";
import { Specifications } from "../Specifications/Specifications";
import { Description } from "../Description/Description";
import { CompareModels } from "../CompareModels/CompareModels";

import styles from "./PageProduct.module.css";

export const PageProduct = () => {
  return (
    <>
      <div className={styles.productWrapper}>
        <div className={styles.header_fixed}>
          <Header />
        </div>
        <div className={styles.wrapper}>
          <div className={styles.conteiner}>
            <Breadcrumbs />
            <main>
              <section className={styles.productNameWrapper}>
                <ProductName />
              </section>
              <ProductPhoto />
              <section
                className={cn(
                  styles.specifications,
                  styles.specificationsWrapper
                )}
              >
                <div className={styles.content}>
                  <div className={styles.productContent}>
                    <ColorsList />
                    <MemoryConfList />
                    <Specifications />
                    <Description />
                    <CompareModels />
                  </div>
                  <ReviewList />
                  <Form />
                </div>
                <Sidebar />
              </section>
            </main>
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};
