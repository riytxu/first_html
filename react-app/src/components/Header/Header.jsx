import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import cn from "classnames";

import styles from "./Header.module.css";
import globalStyles from "../PageProduct/PageProduct.module.css";

import { Logo } from "./../../images";

export const Header = () => {
  const stateCart = useSelector((state) => state.cart.products.length);
  const stateFavorite = useSelector((state) => state.favorite.products.length);

  return (
    <header className={styles.header}>
      <div
        className={cn(
          styles.header__items,
          globalStyles.conteiner,
          globalStyles.wrapper
        )}
      >
        <Link to="/">
          <div className={styles.logo}>
            <img className={styles.logo__img} src={Logo} alt="логотип" />
            <h1 className={styles.logo__title}>
              <span className={styles.logo__title_carrot}>Мой</span>Маркет
            </h1>
          </div>
        </Link>

        <div className={styles.iconWrapper}>
          <div
            className={cn(styles.heartIcon, {
              [styles.iconRate]: !!stateFavorite,
            })}
            data-rate={stateFavorite}
          >
            <svg className={cn(styles.heartIcon_size, styles.heartIcon_color)}>
              <use xlinkHref="sprite.svg#heart"></use>
            </svg>
          </div>
          <div
            className={cn(styles.basketIcon, {
              [styles.iconRate]: !!stateCart,
            })}
            data-rate={stateCart}
          >
            <svg
              className={cn(styles.basketIcon_size, styles.basketIcon_color)}
            >
              <use xlinkHref="sprite.svg#basket"></use>
            </svg>
          </div>
        </div>
      </div>
    </header>
  );
};
