import { Link } from "../Link/Link";

import styles from "./Specifications.module.css";

export const Specifications = () => {
  return (
    <div>
      <h3 className={styles.specifications__title}>Характеристики товара</h3>
      <ul className={styles.specifications__list}>
        <li className={styles.specifications__item}>
          Экран: <b>6.1</b>
        </li>
        <li className={styles.specifications__item}>
          Встроенная память: <b>128 ГБ</b>
        </li>
        <li className={styles.specifications__item}>
          Операционная система: <b>iOS 15</b>
        </li>
        <li className={styles.specifications__item}>
          Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b>
        </li>
        <li className={styles.specifications__item}>
          Процессор:{" "}
          <Link
            href={"https://ru.wikipedia.org/wiki/Apple_A15"}
            target={"_blank"}
            rel={"noreferrer"}
          >
            <b>Apple A15 Bionic</b>
          </Link>
        </li>
        <li className={styles.specifications__item}>
          Вес: <b>173 г</b>
        </li>
      </ul>
    </div>
  );
};
