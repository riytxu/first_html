import { useState } from "react";

import styles from "./MemoryConfList.module.css";

const MemoryConf = ({ memoryNum, onClick }) => {
  return (
    <>
      <input
        className={styles.radioMemory}
        type="radio"
        name="memory-radio"
        id={`${memoryNum}`}
      />
      <label
        onClick={onClick}
        className={styles.radioMemory__label}
        htmlFor={`${memoryNum}`}
      >
        {`${memoryNum}`} ГБ
      </label>
    </>
  );
};

export const MemoryConfList = () => {
  const memory = [128, 256, 512];
  const [activedMemory, setActivedMemory] = useState(128);

  const handleClick = (item) => {
    setActivedMemory(item);
  };

  return (
    <div className={styles.memoryConfiguration}>
      <h3 className={styles.memoryConfiguration__title}>
        Конфигурация памяти: {activedMemory} ГБ
      </h3>
      <div className={styles.memoryConfiguration__list}>
        {memory.map((item) => (
          <MemoryConf
            memoryNum={item}
            key={item}
            onClick={() => handleClick(item)}
          />
        ))}
      </div>
    </div>
  );
};
