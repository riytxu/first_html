import React from "react";

import cn from "classnames";

import styles from "./Review.module.css";

import {
  reviewPhoto1,
  reviewPhoto2,
  reviewRate1,
  reviewRate2,
} from "./../../images";

const reviewsObj = [
  {
    imageAutor: reviewPhoto1,
    firstName: "Марк",
    surname: "Г.",
    rate: reviewRate1,
    titleRate: "оценка 5 звёзд",
    experience: "менее месяца",
    advantage: `это мой первый айфон после после огромного количества телефонов на андроиде. 
        всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, 
        ширик тоже на высоте.`,
    disadvantage: `к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь 
        а если нужно переносить фото с компа, то это только через itunes, который урезает качество 
        фотографий исходное`,
  },
  {
    imageAutor: reviewPhoto2,
    firstName: "Андрей",
    surname: "З.",
    rate: reviewRate2,
    titleRate: "оценка 4 звёзд",
    experience: "менее месяца",
    advantage: `OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго`,
    disadvantage: `Плохая ремонтопригодность`,
  },
];

const ReviewItem = ({
  imageAutor,
  firstName,
  surname,
  rate,
  titleRate,
  experience,
  advantage,
  disadvantage,
}) => {
  return (
    <div className={styles.reviewsList__block}>
      <img
        className={styles.imageAutor}
        src={imageAutor}
        alt="фотография автора"
      />
      <div className={styles.reviewsContent}>
        <div className={styles.reviewsContent__header}>
          <h3 className={styles.reviewsContent__title}>
            {firstName + " " + surname}
          </h3>
          <img
            className={styles.reviewsContent__score}
            src={rate}
            alt={titleRate}
          />
        </div>
        <div
          className={cn(
            styles.reviewsContent__info,
            styles.reviewsContent__info_desctop
          )}
        >
          <p className={styles.reviewsContent__text}>
            <span className={styles.reviewsContent__text_bold}>
              Опыт использования:
            </span>
            {experience}
          </p>
          <p className={styles.reviewsContent__text}>
            <span className={styles.reviewsContent__text_bold}>
              Достоинства:
            </span>
            <br />
            {advantage}
          </p>
          <p className={styles.reviewsContent__text}>
            <span className={styles.reviewsContent__text_bold}>
              Недостатки:
            </span>
            <br />
            {disadvantage}
          </p>
        </div>
      </div>
      <div
        className={cn(
          styles.reviewsContent__info,
          styles.reviewsContent__info_mobile
        )}
      >
        <p className={styles.reviewsContent__text}>
          <span className={styles.reviewsContent__text_bold}>
            Опыт использования:
          </span>
          {experience}
        </p>
        <p className={styles.reviewsContent__text}>
          <span className={styles.reviewsContent__text_bold}>Достоинства:</span>
          <br />
          {advantage}
        </p>
        <p className={styles.reviewsContent__text}>
          <span className={styles.reviewsContent__text_bold}>Недостатки:</span>
          <br />
          {disadvantage}
        </p>
      </div>
    </div>
  );
};

export const ReviewList = () => {
  return (
    <div className={styles.reviews}>
      <div className={styles.reviewsTitle}>
        <div className={styles.reviewsTitle__text}>
          <h2 className={styles.reviewsTitle__text_bold}>Отзывы</h2>
          <span className={styles.reviewsTitle__text_colorGray}>
            {reviewsObj.length}
          </span>
        </div>
      </div>
      <div className={styles.reviewsList}>
        {reviewsObj.map((item, index) => (
          <React.Fragment key={index}>
            <ReviewItem
              imageAutor={item.imageAutor}
              firstName={item.firstName}
              surname={item.surname}
              rate={item.rate}
              titleRate={item.titleRate}
              experience={item.experience}
              advantage={item.advantage}
              disadvantage={item.disadvantage}
            />
            {index < reviewsObj.length - 1 && (
              <div className={styles.separatorWrapper}>
                <div className={styles.separator}></div>
              </div>
            )}
          </React.Fragment>
        ))}
      </div>
    </div>
  );
};
