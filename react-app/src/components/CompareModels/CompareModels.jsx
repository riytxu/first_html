import cn from "classnames";

import styles from "./CompareModels.module.css";

export const CompareModels = () => {
  return (
    <div className={styles.compareModels}>
      <h2 className={styles.compareModels__title}>Сравнение моделей</h2>
      <table
        className={cn(styles.compareModels__table, styles.compareModels__elem)}
      >
        <thead>
          <tr>
            <th className={styles.compareModels__elem}>Модель</th>
            <th className={styles.compareModels__elem}>Вес</th>
            <th className={styles.compareModels__elem}>Высота</th>
            <th className={styles.compareModels__elem}>Ширина</th>
            <th className={styles.compareModels__elem}>Толщина</th>
            <th className={styles.compareModels__elem}>Чип</th>
            <th className={styles.compareModels__elem}>Объём памяти</th>
            <th className={styles.compareModels__elem}>Аккумулятор</th>
          </tr>
        </thead>
        <tbody>
          <tr className={styles.compareModels__tr}>
            <td className={styles.compareModels__td}>Iphone 11</td>
            <td className={styles.compareModels__td}>194 грамма</td>
            <td className={styles.compareModels__td}>150.9 мм</td>
            <td className={styles.compareModels__td}>75.7 мм</td>
            <td className={styles.compareModels__td}>8.3 мм</td>
            <td className={styles.compareModels__td}>A13 Bionic chip</td>
            <td className={styles.compareModels__td}>до 128 Гб</td>
            <td className={styles.compareModels__td}>До 17 часов</td>
          </tr>
          <tr className={styles.compareModels__tr}>
            <td className={styles.compareModels__td}>Iphone 12</td>
            <td className={styles.compareModels__td}>164 грамма</td>
            <td className={styles.compareModels__td}>146.7 мм</td>
            <td className={styles.compareModels__td}>71.5 мм</td>
            <td className={styles.compareModels__td}>7.4 мм</td>
            <td className={styles.compareModels__td}>A14 Bionic chip</td>
            <td className={styles.compareModels__td}>до 256 Гб</td>
            <td className={styles.compareModels__td}>До 19 часов</td>
          </tr>
          <tr className={styles.compareModels__tr}>
            <td className={styles.compareModels__td}>Iphone 13</td>
            <td className={styles.compareModels__td}>174 грамма</td>
            <td className={styles.compareModels__td}>146.7 мм</td>
            <td className={styles.compareModels__td}>71.5 мм</td>
            <td className={styles.compareModels__td}>7.65 мм</td>
            <td className={styles.compareModels__td}>A15 Bionic chip</td>
            <td className={styles.compareModels__td}>до 512 Гб</td>
            <td className={styles.compareModels__td}>До 19 часов</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
