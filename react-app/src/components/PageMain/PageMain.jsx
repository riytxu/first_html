import { Header } from "../Header/Header";
import { Footer } from "../Footer/Footer";
import { Link } from "react-router-dom";

import styles from "./PageMain.module.css";
import linkStyles from "../Link/Link.module.css";

export const PageMain = () => {
  return (
    <div className={styles.wrapper}>
      <Header />
      <main className={styles.content}>
        <p>
          Здесь должно быть содержимое главной страницы.
          <br />
          Но в рамках курса главная страница используется лишь
          <br />в демонстрационных целях
        </p>
        <Link className={linkStyles.link} to="/product">
          Перейти на страницу товара
        </Link>
      </main>
      <Footer />
    </div>
  );
};
