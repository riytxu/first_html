import React, { useEffect, useState } from "react";
import cn from "classnames";

import styles from "./Form.module.css";

export const Form = () => {
  const getStorageItem = (nameStorage) =>
    localStorage.getItem(nameStorage) === null
      ? ""
      : localStorage.getItem(nameStorage);

  const [valueText, setValueText] = useState(getStorageItem("FormValueText"));
  const [valueNumber, setValueNumber] = useState(
    getStorageItem("FormValueNumber")
  );
  const [valueTextarea, setValueTextarea] = useState(
    getStorageItem("FormValueTextarea")
  );
  const [textError, setTextError] = useState(false);
  const [numberError, setNumberError] = useState(false);
  const [dataTextError, setDataTextError] = useState("");
  const [dataNumberError, setDataNumberError] = useState("");

  useEffect(() => {
    localStorage.setItem("FormValueText", valueText);
  }, [valueText]);

  useEffect(() => {
    localStorage.setItem("FormValueNumber", valueNumber);
  }, [valueNumber]);

  useEffect(() => {
    localStorage.setItem("FormValueTextarea", valueTextarea);
  }, [valueTextarea]);

  const handlerForm = (e) => {
    e.preventDefault();
    validateForm(e);
  };

  const validateForm = (e) => {
    let alertStatus = false;

    if (valueText.trim().length < 2 && !alertStatus) {
      alertStatus = true;
      setTextError(true);
      setValueText("");
      !valueText.trim()
        ? setDataTextError("Вы забыли указать имя и фамилию")
        : setDataTextError("Имя не может быть короче 2-х символов");
    } else {
      setTextError(false);
    }

    if ((+valueNumber.trim() < 1 || +valueNumber.trim() > 5) && !alertStatus) {
      alertStatus = true;
      setNumberError(true);
      setValueNumber("");
      setDataNumberError("Оценка должна быть от 1 до 5");
    } else {
      setNumberError(false);
    }

    if (!alertStatus) {
      submitForm(e);
    }
  };

  const submitForm = (e) => {
    localStorage.removeItem("FormValueText");
    localStorage.removeItem("FormValueNumber");
    localStorage.removeItem("FormValueTextarea");
    e.target.submit();
  };

  return (
    <div className={styles.form}>
      <form className={styles.blockForm} noValidate onSubmit={handlerForm}>
        <h2 className={styles.blockForm__title}>Добавить свой отзыв</h2>
        <div className={styles.blockForm__colomn}>
          <div className={styles.blockForm__row}>
            <label
              className={cn(styles.blockForm__text, {
                [styles.errorLable]: textError,
              })}
              data-error={dataTextError}
            >
              <input
                className={cn(styles.blockForm__elem, {
                  [styles.errorInput]: textError,
                })}
                type="text"
                required
                placeholder="Имя и Фамилия"
                value={valueText}
                onChange={(e) => setValueText(e.target.value)}
                onFocus={() => setTextError(false)}
              />
            </label>
            <label
              className={cn(styles.blockForm__number, {
                [styles.errorLable]: numberError,
              })}
              data-error={dataNumberError}
            >
              <input
                className={cn(styles.blockForm__elem, {
                  [styles.errorInput]: numberError,
                })}
                type="number"
                required
                placeholder="Оценка"
                min="0"
                max="5"
                value={valueNumber}
                onChange={(e) => setValueNumber(e.target.value)}
                onFocus={() => setNumberError(false)}
              />
            </label>
          </div>
          <textarea
            className={cn(styles.blockForm__textarea, styles.blockForm__elem)}
            type="text"
            placeholder="Текс отзыва"
            value={valueTextarea}
            onChange={(e) => setValueTextarea(e.target.value)}
          ></textarea>
          <button type="submit" className={styles.blockForm__button}>
            Отправить отзыв
          </button>
        </div>
      </form>
    </div>
  );
};
