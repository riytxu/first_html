import React from "react";

import styles from "./Link.module.css";

export const Link = ({ href, rel, target, children }) => {
  return (
    <a className={styles.link} href={href} rel={rel} target={target}>
      {children}
    </a>
  );
};
