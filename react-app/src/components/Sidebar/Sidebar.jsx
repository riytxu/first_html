import { useState, useCallback, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addToCart, removeFromCart } from "../../reducers/cart-reducer";
import {
  addTofavorite,
  removeFromFavorite,
} from "../../reducers/favorite-reducer";
import cn from "classnames";

import { Iframe } from "../Iframe/Iframe";
import styles from "./Sidebar.module.css";

const currentSalary = (price, sale) => {
  let result = (price * (100 - sale)) / 100;
  result = (result.toFixed(0) / 10).toFixed(0) * 10;
  return result;
};

const addSpace = (price) => {
  const toArrPrice = `${price}`.split("");
  toArrPrice.splice(2, 0, " ");
  const result = toArrPrice.join("");
  return result;
};

export const Sidebar = () => {
  const product = { id: 1234, name: "Iphone 13", salary: 75990, sale: 8 };

  // написал дополнительыне функции чтобы принимать с бд
  // цену и скидку и отображать актуальыне данные на странице
  // чтобы избежать дополнительного рендера добавил мемоизацию
  const newSalary = useMemo(() => {
    return addSpace(currentSalary(product.salary, product.sale));
  }, [product.salary, product.sale]);
  const oldSalary = useMemo(() => {
    return addSpace(product.salary);
  }, [product.salary]);

  const dispatch = useDispatch();

  const hasProductInCart = useSelector((state) => {
    return state.cart.products.some((item) => item?.id === product.id);
  });
  const hasProductInFavorite = useSelector((state) => {
    return state.favorite.products.some((item) => item?.id === product.id);
  });

  const [buttonStatus, setButtonStatus] = useState(hasProductInCart);
  const [buttonContent, setButtonContent] = useState(() => {
    return hasProductInCart ? "Товар уже в корзине" : "Добавить в корзину";
  });
  const [inputStatus, setInputStatus] = useState(hasProductInFavorite);

  // мемоизировал чтобы избежать дополнительного рендера
  // при возникновении других событий в компоненте
  const memoHandlerButton = useCallback(() => {
    return handlerButton();
  }, [buttonStatus]);

  const handlerButton = () => {
    if (!buttonStatus) {
      dispatch(addToCart(product));
      setButtonStatus(true);
      setButtonContent("Товар уже в корзине");
    } else {
      dispatch(removeFromCart(product));
      setButtonStatus(false);
      setButtonContent("Добавить в корзину");
    }
  };

  // мемоизировал чтобы избежать дополнительного рендера
  // при возникновении других событий в компоненте
  const memoHandlerInput = useCallback(() => {
    return handlerInput();
  }, [inputStatus]);

  const handlerInput = () => {
    if (!inputStatus) {
      dispatch(addTofavorite(product));
      setInputStatus(true);
    } else {
      dispatch(removeFromFavorite(product));
      setInputStatus(false);
    }
  };

  return (
    <div className={styles.sidebar}>
      <div className={styles.priceBlock}>
        <div className={styles.priceWrapper}>
          <div className={styles.price}>
            <div className={styles.price__oldPrice}>
              <h3 className={cn(styles.price__oldPriceValue, styles.oldPrice)}>
                {`${oldSalary}₽`}
              </h3>
              <div className={styles.price__sale}>
                <h3
                  className={styles.price__saleValue}
                >{`-${product.sale}%`}</h3>
              </div>
            </div>
            <h3 className={styles.price__newPrice}>{`${newSalary}₽`}</h3>
          </div>
          <label className={styles.heart} htmlFor="heart__input">
            <input
              type="checkbox"
              id="heart__input"
              checked={inputStatus}
              onChange={memoHandlerInput}
            />
            <svg className={styles.heart_color}>
              <use xlinkHref="sprite.svg#heart"></use>
            </svg>
          </label>
        </div>
        <div className={styles.delivery}>
          <h3 className={styles.delivery__title}>
            Самовывоз в четверг, 1 сентября —{" "}
            <span className={styles.delivery__title_bold}>бесплатно</span>
          </h3>
          <h3 className={styles.delivery__title}>
            Курьером в четверг, 1 сентября —{" "}
            <span className={styles.delivery__title_bold}>бесплатно</span>
          </h3>
        </div>
        <button
          className={cn(styles.addButton, styles.basket, {
            [styles.addButton_active]: buttonStatus,
          })}
          onClick={memoHandlerButton}
        >
          {buttonContent}
        </button>
      </div>
      <div className={styles.adsBlock}>
        <p className={styles.adsBlock__title}>Реклама</p>
        <div className={styles.adsBlock__iframe}>
          <Iframe
            className={styles.ads}
            src={"/Iframe/ads.html"}
            height={"400"}
            title={"У вас не работает iframe"}
          />
          <Iframe
            className={styles.ads}
            src={"/Iframe/ads.html"}
            height={"400"}
            title={"У вас не работает iframe"}
          />
        </div>
      </div>
    </div>
  );
};
