import style from "./ProductName.module.css";

export const ProductName = () => {
  return (
    <h2 className={style.productName__title}>
      Смартфон Apple iPhone 13, синий
    </h2>
  );
};
