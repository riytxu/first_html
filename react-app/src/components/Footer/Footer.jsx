import React from "react";
import { useCurrentDate } from "@kundinos/react-hooks";
import cn from "classnames";

import { Link } from "./../Link/Link";

import globalStyles from "../PageProduct/PageProduct.module.css";
import styles from "./Footer.module.css";

export const Footer = () => {
  const currentDate = useCurrentDate();
  const currentYear = currentDate.getFullYear();

  return (
    <footer className={styles.footer}>
      <div className={cn(globalStyles.conteiner, globalStyles.wrapper)}>
        <div className={styles.footer__block}>
          <p className={styles.footer__title}>
            <span
              className={cn(
                styles.footer__titleMain,
                styles.footer__title_bold
              )}
            >
              © ООО «<span className={styles.footer__title_carrot}>Мой</span>
              Маркет»,
              {`2018-${currentYear}`}
            </span>
            <span className={styles.footer__title}>
              Для уточнения информации звоните по номеру{" "}
              <Link href={"tel:79000000000"}>
                +7&nbsp;900&nbsp;000&nbsp;0000
              </Link>
              ,&nbsp;
            </span>
            <span className={styles.footer__title}>
              а предложения по сотрудничеству отправляйте на почту{" "}
              <Link href={"mailto:partner@mymarket.com"}>
                partner@mymarket.com
              </Link>
            </span>
          </p>
          <Link href={"#top"}>Наверх</Link>
        </div>
      </div>
    </footer>
  );
};
