import { configureStore, combineReducers } from "@reduxjs/toolkit";
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import storage from "redux-persist/lib/storage";

import cartReducer from "./reducers/cart-reducer";
import favoriteReducer from "./reducers/favorite-reducer";

const logger = (store) => (next) => (action) => {
  console.log("action", action);
  const result = next(action);
  console.log("next state", store.getState());
  return result;
};

const counter = [];
const counterActions = (store) => (next) => (action) => {
  counter.push(action);
  console.log(`Количество обработанных действий: ${counter.length}`);
  console.log(store.getState());
  const result = next(action);
  return result;
};

const rootReducer = combineReducers({
  cart: cartReducer,
  favorite: favoriteReducer,
});

const persistConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(counterActions, logger),
});

export const persistor = persistStore(store);
