import { createSlice } from "@reduxjs/toolkit";

export const favoriteSlice = createSlice({
  name: "favorite",

  initialState: {
    products: [],
  },

  reducers: {
    addTofavorite: (prevState, action) => {
      return {
        ...prevState,
        products: [...prevState.products, action.payload],
      };
    },
    removeFromFavorite: (prevState, action) => {
      return {
        ...prevState,
        products: [
          ...prevState.products.filter((item) => {
            return item.id !== action.payload.id;
          }),
        ],
      };
    },
  },
});

export const { addTofavorite, removeFromFavorite } = favoriteSlice.actions;
export default favoriteSlice.reducer;
