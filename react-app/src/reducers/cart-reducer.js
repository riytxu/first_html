import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice({
  name: "cart",

  initialState: {
    products: [],
  },

  reducers: {
    addToCart: (prevState, action) => {
      return {
        ...prevState,
        products: [...prevState.products, action.payload],
      };
    },
    removeFromCart: (prevState, action) => {
      return {
        ...prevState,
        products: [
          ...prevState.products.filter((item) => {
            return item.id !== action.payload.id;
          }),
        ],
      };
    },
  },
});

export const { addToCart, removeFromCart } = cartSlice.actions;
export default cartSlice.reducer;
