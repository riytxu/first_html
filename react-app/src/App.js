import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import { PageMain } from "./components/PageMain/PageMain";
import { PageNotFound } from "./components/PageNotFound/PageNotFound";
import { PageProduct } from "./components/PageProduct/PageProduct";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<PageMain />} />
        <Route path="/product" element={<PageProduct />} />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
