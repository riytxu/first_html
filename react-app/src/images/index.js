import Logo from "./Header/logo.svg";
import ProductPhoto1 from "./PageProduct/productPhoto/image-1.webp";
import ProductPhoto2 from "./PageProduct/productPhoto/image-2.webp";
import ProductPhoto3 from "./PageProduct/productPhoto/image-3.webp";
import ProductPhoto4 from "./PageProduct/productPhoto/image-4.webp";
import ProductPhoto5 from "./PageProduct/productPhoto/image-5.webp";

import colorIphone1 from "./PageProduct/colorIphone/color1.png";
import colorIphone2 from "./PageProduct/colorIphone/color2.png";
import colorIphone3 from "./PageProduct/colorIphone/color3.png";
import colorIphone4 from "./PageProduct/colorIphone/color4.png";
import colorIphone5 from "./PageProduct/colorIphone/color5.png";
import colorIphone6 from "./PageProduct/colorIphone/color6.png";

import reviewPhoto1 from "./PageProduct/reviews/review-1.jpeg";
import reviewPhoto2 from "./PageProduct/reviews/review-2.jpeg";
import reviewRate1 from "./PageProduct/reviews/star-5.svg";
import reviewRate2 from "./PageProduct/reviews/star-4.svg";

export {
  Logo,
  ProductPhoto1,
  ProductPhoto2,
  ProductPhoto3,
  ProductPhoto4,
  ProductPhoto5,
  colorIphone1,
  colorIphone2,
  colorIphone3,
  colorIphone4,
  colorIphone5,
  colorIphone6,
  reviewPhoto1,
  reviewPhoto2,
  reviewRate1,
  reviewRate2,
};
