'use strict'

// Exercise 1

let user = {};

console.log(isEmpty(user)); // true

user.age = 12;

console.log(isEmpty(user)); // false

/**
* Возвращает булевый результат проверки наличия\отсутствия свойств у объекта
*
* @param {object} obj - объект проверки
* @return {boolean} возвращает результат проверки
*/

function isEmpty(obj) {
   return !Object.keys(obj).length;
}

// Exercise 3

let salaries = {
   Ann: 160000,
   John: 100000,
   Pete: 130000,
};

/**
* Изменяет в процентах величину зарплат объекта salaries
*
* @param {number} perzent - процент изменения (может быть отрицательным)
* @return {object} возвращает измененный объект salaries
*/

function raiseSalary(perzent) {
   let sumSalaries = 0;
   let copySalaries = JSON.parse(JSON.stringify(salaries))
   for (let item in copySalaries) {
      copySalaries[item] = Math.floor(copySalaries[item] / 100 * (100 + perzent));
      sumSalaries += copySalaries[item];
   }
   console.log(sumSalaries);
   return copySalaries;
}

console.log(raiseSalary(5))
console.log(salaries)
