'use strict'

// Exercise 1

let arr1 = [1 ,2, 10, 5]
let arr2 = ["a", {}, 3, 3, -2]

function getSumm(arr) {
    let result = 0
    arr.forEach(item => {
       if (typeof item === "number")  {
            result += item
       }
    })
    return result
}

console.log(getSumm(arr1)) // 18
console.log(getSumm(arr2)) // 4

// Exercise 3

let cart = [4884]

function addToCart(item) {
    if (!cart.includes(item)) {
        cart.push(item)
    } else {
        console.log('Вы уже добавили данный товар в корзину!')
    }
}

function removeFromCart(item) {
    if (cart.includes(item)) {
        let indexItem = cart.indexOf(item)
        cart.splice(indexItem, 1)
    } else {
        console.log('Данного товара нет в корзине :(')
    }
}

addToCart(3456)
console.log(cart) // [4884, 3456]
addToCart(3456)
console.log(cart) // [4884, 3456]
removeFromCart(4884)
console.log(cart) // [3456]