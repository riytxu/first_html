'use strict'

const form = document.querySelector('.block-form')

class AddReviewForm {
    constructor(form) {
        this.form = form
        this.formBtn = this.form.querySelector('button')
        this.lableName = this.form.querySelector('.block-form__name')
        this.inputName = this.lableName.querySelector('input')
        this.lableRate = this.form.querySelector('.block-form__rate')
        this.inputRate = this.lableRate.querySelector('input')
        this.formTextarea = this.form.querySelector('textarea')
        
        this.setInputsValue()
        this.addEventListener()
    }

    validateForm() {
        this.alertFlag = false
        if (this.inputName.value.trim().length < 2 && !this.alertFlag) {
            this.alertFlag = true
            localStorage.setItem('FormNameValue', this.inputName.value)
            this.inputName.style.border = '1px solid var(--dark--carrot)'
            this.lableName.classList.add('error-input')
            if (!this.inputName.value.trim()) {
                this.inputName.value = ''
                this.lableName.setAttribute('data-error', 'Вы забыли указать имя и фамилию')
            } else {
                this.inputName.value = ''
                this.lableName.setAttribute('data-error', 'Имя не может быть короче 2-х символов')
            }
            this.inputName.addEventListener('focus', this.checkInput.bind(this))
        } else {
            this.lableName.classList.remove('error-input')
            this.inputName.style.border = '1px solid var(--dark-gray)'
            this.inputName.removeEventListener('focus', this.checkInput.bind(this))
        }

        if ((+this.inputRate.value.trim() < 1 || +this.inputRate.value.trim() > 5) && !this.alertFlag) {
            this.alertFlag = true
            this.inputRate.value = ''
            localStorage.setItem('FormRateValue', this.inputRate.value)
            this.inputRate.style.border = '1px solid var(--dark--carrot)'
            this.lableRate.classList.add('error-input')
            this.lableRate.setAttribute('data-error', 'Оценка должна быть от 1 до 5')
            this.inputRate.addEventListener('focus', this.checkInput.bind(this))
        } else {
            this.lableRate.classList.remove('error-input')
            this.inputRate.style.border = '1px solid var(--dark-gray)'
            this.inputRate.removeEventListener('focus', this.checkInput.bind(this))
        }

        if (!this.alertFlag) { 
            this.submitForm() 
        }
    }

    addEventListener() {
        this.inputName.addEventListener('input', this.handlerInpusForm.bind(this))
        this.inputRate.addEventListener('input', this.handlerInpusForm.bind(this))
        this.formTextarea.addEventListener('input', this.handlerInpusForm.bind(this))
        this.formBtn.addEventListener('click', (e) => {
            e.preventDefault()
            this.validateForm()
        })
    }

    handlerInpusForm() {
        localStorage.setItem('FormNameValue', this.inputName.value)
        localStorage.setItem('FormRateValue', this.inputRate.value)
        localStorage.setItem('FormTextareaValue', this.formTextarea.value)
    }

    setInputsValue() {
        this.inputName.value = localStorage.getItem('FormNameValue')
        this.inputRate.value = localStorage.getItem('FormRateValue')
        this.formTextarea.value = localStorage.getItem('FormTextareaValue')
    }

    checkInput() {
        this.lableName.classList.remove('error-input')
        this.inputName.style.border = '1px solid var(--dark-gray)'
        this.lableRate.classList.remove('error-input')
        this.inputRate.style.border = '1px solid var(--dark-gray)'
    }

    submitForm() {
        localStorage.removeItem('FormNameValue')
        localStorage.removeItem('FormRateValue')
        localStorage.removeItem('FormTextareaValue')
        this.form.submit()
    }
}

new AddReviewForm(form)
