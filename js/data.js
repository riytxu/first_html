'use strict'

// Exercise 2

let selectGoods = {
    get name() {
       return `Смартфон Apple iPhone 13, ${this.color}`
    },
    color: 'синий',
    memoty: 128,
    screenSize: 6.1,
    operationsSystem: 'iOS 15',
    interfaces: ['NFC', 'Bluetooth', 'Wi-Fi'],
    processor: 'Apple A15 Bionic',
    weight: 173,
    description: [
        `Наша самая совершенная система двух камер.
        Особый взгляд на прочность дисплея.
        Чип, с которым всё супербыстро.
        Аккумулятор держится заметно дольше.
        iPhone 13 - сильный мира всего.`,
        `Мы разработали совершенно новую схему расположения
         и развернули объективы на 45 градусов. Благодаря этому
         внутри корпуса поместилась нашалучшая система двух камер
         с увеличенной матрицей широкоугольной камеры. Кроме того,
         мы освободили место для системы оптической стабилизациии
         зображения сдвигом матрицы. И повысили скорость работы
         матрицы на сверхширокоугольной камере.`,
        `Новая сверхширокоугольная камера видит больше деталей в
         тёмных областяхснимков. Новая широкоугольная камера улавливает
         на 47% больше света для более качественных фотографий и видео. 
         Новая оптическая стабилизация сосдвигом матрицы обеспечит 
         чёткие кадры даже в неустойчивом положении.`,
        `Режим «Киноэффект» автоматически добавляет великолепные
         эффекты перемещения фокуса и изменения резкости. Просто
         начните запись видео. Режим «Киноэффект» будет удерживать
         фокус на объекте съёмки, создавая красивый эффект
         размытия вокруг него. Режим «Киноэффект» распознаёт, когда
         нужно перевести фокус на другого человека или объект,
         который появился в кадре. Теперь ваши видео будут
         смотреться как настоящее кино.`
    ],
    priceRub: 67990,
    discount: 8,
    goodsImages: [
        "./images/color1.png",
        "./images/color2.png",
        "./images/color3.png",
        "./images/color4.png",
        "./images/color5.png"
    ],
    goodsColors: [
        'красный',
        'зелёный',
        'розовый',
        'синий',
        'белый',
        'чёрный'
    ],
    goodsMemory: [
        '128',
        '256',
        '512'
    ],
    delivery: [
        {
            name: 'Самовывоз',
            data: 'четверг, 1 сентября',
            cost: 0
        },
        {
            name: 'Курьером',
            data: 'четверг, 1 сентября',
            cost: 0
        }
    ]
};

let reviewsFirst = {
    image: './images/review-1.jpeg',
    firstname: 'Марк',
    surname: 'Г.',
    evaluation: 5,
    mainContent: {
        usegeTime: 'менее месяца',
        advantage: `это мой первый айфон после после огромного количества
         телефонов на андроиде. всё плавно, чётко и красиво. довольно
         шустрое устройство. камера весьма неплохая, ширик тоже на высоте.`,
        disadvantages: `к самому устройству мало имеет отношение, но перенос
         данных с андроида - адская вещь) а если нужно переносить фото с
         компа, то это только через itunes, который урезает качество
         фотографий исходное`
        }
}

let reviewsSecond = {
    image: './images/review-2.jpeg',
    firstname: 'Марк',
    surname: 'Г.',
    evaluation: 4,
    mainContent: {
        usegeTime: 'менее месяца',
        advantage: `OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго`,
        disadvantages: `Плохая ремонтопригодность`
        }
}
