'use strict'

// Exercise 1

const promise = new Promise((resolve, reject) => {
    const answer = prompt('Введите положительное число => ')
    const number = Number(answer)
    if (typeof number === 'number' && !isNaN(number) && number > 0) {
        resolve(number)
    } else {
        reject(answer)
    }
})

promise
    .then((num) => {
        let timer = setInterval(() => {
            if (num >= 1) {
                console.log(`Осталось ${num--}`)
            } else {
                console.log('Время вышло!')
                clearInterval(timer)
            }
        }, 1000)
    
    })
    .catch((err) => {
        console.log(new Error(`'${err}' не явлется числом, либо <= 0!`))
    })

// Exercise 2

const startTime = Date.now()
fetch('https://reqres.in/api/users')
    .then((response) => {
        const endTime = Date.now() - startTime
        console.log(`Запрос выполнялся ${endTime} миллисекунд(ы)`)
        return response.json() 
    })
    .then((data) => {
        parseDate(data.data)
    })
    .catch((err) => {
        console.log(new Error(err))
    })

function parseDate(data) {
    console.log(`Получили пользователей: ${data.length}`)
    data.forEach((item) => {
        console.log(`${item.first_name} ${item.last_name} (${item.email})`)
    })
}