'use strict'

/* Валидация и отправка формы */

const form = document.querySelector('.block-form')
const formTextarea = form.querySelector('textarea')
const formBtn = document.querySelector('.block-form__button')
const lableName = document.querySelector('.block-form__name')
const inputName = lableName.querySelector('input')
const lableRate = document.querySelector('.block-form__rate')
const inputRate = lableRate.querySelector('input')

function setInputsValue () {
    inputName.value = localStorage.getItem('FormNameValue')
    inputRate.value = localStorage.getItem('FormRateValue')
    formTextarea.value = localStorage.getItem('FormTextareaValue')
}
setInputsValue()

inputName.addEventListener('input', handlerInpusForm)
inputRate.addEventListener('input', handlerInpusForm)
formTextarea.addEventListener('input', handlerInpusForm)

function handlerInpusForm() {
    localStorage.setItem('FormNameValue', inputName.value)
    localStorage.setItem('FormRateValue', inputRate.value)
    localStorage.setItem('FormTextareaValue', formTextarea.value)
}

formBtn.addEventListener('click', (e) => {
    e.preventDefault()
    validateForm()
})

function validateForm() {

    let alertFlag = false

    if (inputName.value.trim().length < 2 && !alertFlag) {
        alertFlag = true
        localStorage.setItem('FormNameValue', inputName.value)
        inputName.style.border = '1px solid var(--dark--carrot)'
        lableName.classList.add('error-input')
        if (!inputName.value.trim()) {
            inputName.value = ''
            lableName.setAttribute('data-error', 'Вы забыли указать имя и фамилию')
        } else {
            inputName.value = ''
            lableName.setAttribute('data-error', 'Имя не может быть короче 2-х символов')
        }
        inputName.addEventListener('focus', checkInput)
    } else {
        lableName.classList.remove('error-input')
        inputName.style.border = '1px solid var(--dark-gray)'
        inputName.removeEventListener('focus', checkInput)
    }

    if ((+inputRate.value.trim() < 1 || +inputRate.value.trim() > 5) && !alertFlag) {
        alertFlag = true
        inputRate.value = ''
        localStorage.setItem('FormRateValue', inputRate.value)
        inputRate.style.border = '1px solid var(--dark--carrot)'
        lableRate.classList.add('error-input')
        lableRate.setAttribute('data-error', 'Оценка должна быть от 1 до 5')
        inputRate.addEventListener('focus', checkInput)
    } else {
        lableRate.classList.remove('error-input')
        inputRate.style.border = '1px solid var(--dark-gray)'
        inputRate.removeEventListener('focus', checkInput)
    }

    if (!alertFlag) { submitForm() }

    function checkInput() {
        lableName.classList.remove('error-input')
        inputName.style.border = '1px solid var(--dark-gray)'
        lableRate.classList.remove('error-input')
        inputRate.style.border = '1px solid var(--dark-gray)'
    }
}

function submitForm() {
    localStorage.removeItem('FormNameValue')
    localStorage.removeItem('FormRateValue')
    localStorage.removeItem('FormTextareaValue')
    form.submit()
}

/* Корзина и избранные товары */

{
    const heardIcon = document.querySelector('.heart-icon')
    const likeGoodsInput = document.querySelector('#heart__input')
    const basketIcon = document.querySelector('.basket-icon')
    const addGoodsBtn = document.querySelector('.add-button') 

    const basketArr = localStorage.getItem('basketArr') === null ?
        [] : JSON.parse(localStorage.getItem('basketArr'))
    const likeArr = localStorage.getItem('likeArr') === null ?
        [] : JSON.parse(localStorage.getItem('likeArr'))

    if (!!basketArr.length) { getStatusDelGoods() }
    if (!!likeArr.length) { getStatusDelLike() }

    addGoodsBtn.addEventListener('click', basketToggle)
    likeGoodsInput.addEventListener('change', likeToggle)
    
    function basketToggle() {
        if (!!basketArr.length) {
            basketArr.pop()
            getStatusAddGoods()
            localStorage.removeItem('basketArr')
        } else {
            basketArr.push(123)
            getStatusDelGoods()
            localStorage.setItem('basketArr', JSON.stringify(basketArr))
        }
    }

    function getStatusAddGoods() {
        addGoodsBtn.classList.remove('add-to-basket')
        addGoodsBtn.textContent = 'Добавить в корзину'
        basketIcon.classList.remove('icon-rate')
        basketIcon.removeAttribute('data-rate')
    }

    function getStatusDelGoods() {
        addGoodsBtn.classList.add('add-to-basket')
        addGoodsBtn.textContent = 'Товар уже в корзине'
        basketIcon.classList.add('icon-rate')
        basketIcon.setAttribute('data-rate', `${basketArr.length}`)
    }

    function likeToggle() {
        if (!!likeArr.length) {
            likeArr.pop()
            getStatusAddLike()
            localStorage.removeItem('likeArr')
        } else {
            likeArr.push(123)
            getStatusDelLike()
            localStorage.setItem('likeArr', JSON.stringify(likeArr))
        }
    }

    function getStatusAddLike() {
        likeGoodsInput.checked = false
        heardIcon.classList.remove('icon-rate')
        heardIcon.removeAttribute('data-rate')
    }

    function getStatusDelLike() {
        likeGoodsInput.checked = true
        heardIcon.classList.add('icon-rate')
        heardIcon.setAttribute('data-rate', `${likeArr.length}`)
    }
}
    