'use strict'

// Exercise 1

let i = 0
while (i <= 20) {
    if (i % 2 === 0) { console.log(i) };
    i++;
}

// Exercise 2

let sum = 0;
for (let i = 0; i < 3; i++) {
    let num = +prompt('Введите число => ');
    if (isNaN(num)) {
        console.log('Ошибка, выввели не число');
        break;
    } else { sum += num };
};
if (!!sum) { console.log(sum) };

// Exercise 3

function getNameOfMonth(month) {
    let mounthList = [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь'
    ];
    return mounthList[month];
};

for (let i = 0; i < 12; i++) {
    if (getNameOfMonth(i) === 'Октябрь') { continue };
    console.log(getNameOfMonth(i));
}

// Exercise 4

/* 
    IIFE (Immediately Invoked Function Expression) => самовызывающаяся анонимная функция 
    
    (function () {
        some code..... ;
    })();
*/
