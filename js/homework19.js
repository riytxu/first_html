"use strict"

// Exercise 1

let a = '$100';
let b = '300$';
let sum = +a.replace('$', '') + +b.replace('$', '');
console.log(sum); // 400

// Exercise 2

let message = ' привет, медвед      ';
message = message.trim();
message = message[0].toUpperCase() + message.slice(1);

console.log(message); // “Привет, медвед”

// Exercise 3

let age = +prompt('Сколько вам лет?');


switch (true) {
    case age >= 0 && age < 4:
        alert(`Вам ${age} лет и вы младенец`);
        break;
    case age >= 4 && age < 12:
        alert(`Вам ${age} лет и вы ребенок`);
        break;
    case age >= 12 && age < 19:
        alert(`Вам ${age} лет и вы подросток`);
        break;
    case age >= 19 && age < 41:
        alert(`Вам ${age} лет и вы познаёте жизнь`);
        break;
    case age >= 41 && age < 81:
        alert(`Вам ${age} лет и вы познали жизнь`);
        break;
    case age >= 81:
        alert(`Вам ${age} лет и вы долгожитель`);
        break;
    default:
        alert('Нет таких значений');
}

// Exercise 4 

let newMessage = 'Я работаю со строками как профессионал!';
let count = newMessage.split(' ').length;
console.log(count); // Должно быть 6