// Exercise 1

let a='100px';
let b='323px';

let sum = parseFloat(a) + parseFloat(b);

console.log(sum)

// Exercise 2

console.log(Math.max(10, -45, 102, 36, 12, 0, -1))

// Exercise 3

let c = 123.3399; // Округлить до 123
console.log(Math.round(c));

let d = 0.111; // Округлить до 1
console.log(Math.ceil(d));

let e = 45.333333; // Округлить до 45.3
console.log(+e.toFixed(1));

let f = 3; // Возвести в степень 5 (должно быть 243)
console.log(Math.pow(f, 5));

let g = 400000000000000; // Записать в сокращенномвиде
let shortG = 4e15;
console.log(shortG);

let h ='1'!= 1; // Поправить условие, чтобы результат был true (значения изменять нельзя, только оператор)
let trueH = '1'!== 1;
console.log(trueH);

console.log(0.1 + 0.2 === 0.3);// Вернёт false, почему?
console.log((0.1 + 0.2).toFixed(20), (0.3).toFixed(20))
/* 
    из-за представления чисел с плавающей запятой в памяти, можно проверить и убедится что они не равны
    console.log((0.1 + 0.2).toFixed(20), (0.3).toFixed(20))

*/
